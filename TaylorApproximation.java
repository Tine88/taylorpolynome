package RegenbogenPony.TazlorApproximation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 06.11.14.
 */

public class TaylorApproximation {
    List<Double> ableitungen = new ArrayList<Double>();
            List<Double> a = new ArrayList<Double>();
            List<Integer> b = new ArrayList<Integer>();
            int i = 0;

    public TaylorApproximation() {
            ableitungen.add(1.0);
            ableitungen.add(0.0);
            ableitungen.add(-1.0);
            ableitungen.add(0.0);
            ableitungen.add(1.0);
            ableitungen.add(0.0);
            ableitungen.add(-1.0);

        //
            }

    public double fakultaet(int wert) {
            double ergebnis = 1.0;
            if (wert == 0) {
            return 1;
            }

            for (int b = 1; b <= wert; b++) {
            ergebnis *= b;
            }
            System.out.println("fakultate: " + ergebnis);
            return ergebnis;
            }

    public void taylorreihe() {
        int i = 1;
        for (double aktuelleAbleitung : ableitungen) {
            a.add((aktuelleAbleitung / fakultaet(i)));
            b.add(i);

            i++;
        }
        double taylor = 0;
        int index = 0;
        for (int x = -5; x < 5; x++) {
            taylor = 0;
            index = 0;
            for (int count = 0; count < a.size(); count++) {

                if (a.size() == (index)) {
                    break;
                }
                taylor += a.get(index) * Math.pow(x, count + 1);

                ++index;
            }
            System.out.println("Reihe: " + taylor);

        }
    }
// System.out.println(taylor);

    public static void main(String[] args) {
        TaylorApproximation taylor = new TaylorApproximation();
        taylor.taylorreihe();
    }
}
